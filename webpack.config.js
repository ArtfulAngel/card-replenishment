const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
    entry: ['babel-polyfill', './src/index.jsx'],
    output: {
        path: path.resolve('public'),
        filename: 'bundle.js',
    },
    devtool: 'cheap-module-source-map',
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.(css|scss)$/,
                use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'sass-loader' }],
            },
            {
                test: /\.png$/,
                loader: 'url-loader?limit=10000&mimetype=image/png',
            },
            {
                test: /\.svg/,
                loader: 'url-loader?limit=26000&mimetype=image/svg+xml',
            },
        ],
    },
    resolve: {
        alias: {
            components: path.resolve('src/components'),
            pages: path.resolve('src/pages'),
        },
        extensions: ['.js', '.jsx'],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: 'index.html',
        }),
    ],

    devServer: {
        historyApiFallback: true,
        port: 555,
        hot: true,
    },
};
