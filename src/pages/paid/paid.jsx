import React, { Component } from 'react';

import Layout from 'components/layout';
import Modal from 'components/modal';
import Card from 'components/card';
import SumReplenishment from 'components/sum-replenishment';

import './paid.scss';

class Paid extends Component {
    getPropsForCard = () => {
        return {
            numberCard: '5454 4567 8700 9019',
            month: '09',
            year: '2017',
        };
    };

    render() {
        return (
            <Layout type="overlay">
                <Modal className="paid__modal">
                    <div className="paid">
                        <div className="paid__body">
                            <div className="paid__block">
                                <div className="paid__block-title--icon" />
                                <div className="paid__block-title">
                                    <div className="paid__block-title--label">Пополнение карты</div>
                                    <div className="paid__block-title--sub-label">
                                        Накопительная премиальная *9101
                                    </div>
                                </div>
                            </div>
                            <div className="paid__block">
                                <Card {...this.getPropsForCard()} />
                            </div>
                            <div className="paid__block">
                                <SumReplenishment />
                            </div>
                        </div>
                        <div className="paid__footer">
                            <div className="paid__block-total">
                                <div className="paid__block-total--label">Итого к оплате</div>
                                <div className="paid__block-total--sum">
                                    <div className="paid__block-total--sum-bold">0</div>
                                    ,00 ₽
                                </div>
                            </div>
                            <button className="paid__button">Подтвердить </button>
                        </div>
                    </div>
                </Modal>
            </Layout>
        );
    }
}
export default Paid;
