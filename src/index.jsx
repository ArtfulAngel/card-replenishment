import React from 'react';
import ReactDOM from 'react-dom';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import App from './app';
import Paid from 'pages/paid';

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={App} />
            <Route path="/paid" component={Paid} />
        </Switch>
    </BrowserRouter>,
    document.getElementById('root'),
);
