import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './card.scss';

class Card extends Component {
    static propTypes = {
        numberCard: PropTypes.string,
        month: PropTypes.string,
        year: PropTypes.string,
    };

    render() {
        const { numberCard, month, year } = this.props;
        return (
            <div className="card">
                <div className="card__line" />
                <div className="card__block">
                    <div className="card__logo" />
                    <div className="card__block-number">
                        <div className="card__block-number--label">Номер карты</div>
                        <input type="text" className="card__block-number--input" value={numberCard} />
                    </div>
                    <div className="card__block-date">
                        <input type="text" className="card__block-date--input" value={month} />
                        <input type="text" className="card__block-date--input" value={year} />
                        <div className="card__block-date--logo" />
                    </div>
                </div>
            </div>
        );
    }
}

export default Card;
