import React, { Component } from 'react';

import './sum-replenishment.scss';

class SumReplenishment extends Component {
    render() {
        return (
            <div className="sum-replenishment">
                <div className="sum-replenishment__label">Сумма пополнения</div>
                <div className="sum-replenishment__block">
                    <input type="text" className="sum-replenishment__block--input" placeholder="₽" />
                    <div className="sum-replenishment__block--text">
                        {`Комиссия 1% при пополнении
                        менне 3000 Р`}
                    </div>
                </div>
            </div>
        );
    }
}

export default SumReplenishment;
