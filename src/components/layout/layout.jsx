import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './layout.scss';

export default class Layout extends Component {
    static propTypes = {
        className: PropTypes.string,
        type: PropTypes.string.isRequired,
        children: PropTypes.element,
    };

    getComponentClassNames() {
        const { className, type } = this.props;

        return {
            component: classNames(className ? className : '', 'l-layout', type ? `l-layout--${type}` : ''),
        };
    }
    render() {
        const classes = this.getComponentClassNames();

        const { children, type } = this.props;

        return (
            <div className={classes.component}>
                {type === 'overlay' ? <div className="l-layout__wrapper">{children}</div> : children}
            </div>
        );
    }
}
