import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

import './modal.scss';
import { withRouter } from 'react-router-dom';

class Modal extends React.Component {
    static propTypes = {
        history: PropTypes.object.isRequired,
        className: PropTypes.string,
        children: PropTypes.node,
    };

    getComponentClassNames() {
        const { className } = this.props;
        return classNames('b-modal', { [className]: Boolean(className) });
    }

    handlerClose = () => {
        const { history } = this.props;
        history.goBack();
    };

    render() {
        const { children } = this.props;

        return (
            <div className={this.getComponentClassNames()}>
                <div className="b-modal__head">
                    <span className="b-modal__close" onClick={this.handlerClose} />
                </div>
                <div className="b-modal__body">{children}</div>
            </div>
        );
    }
}

export default withRouter(Modal);
