import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './app.scss';

class App extends Component {
    render() {
        return (
            <div className="app">
                <Link to="/paid" className="app__link">
                    Оплатить
                </Link>
            </div>
        );
    }
}

export default App;
